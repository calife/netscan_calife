require 'benchmark'
require 'ipaddr'
require 'pp'

module NetscanCalife

  class IpScan

    def self.scan(network,netmask)

      total_elapsed= Benchmark.measure {

        net1 = IPAddr.new(network).mask(netmask)

        statuses=net1.to_range.map do |host|

          pid=fork {
            exec("ping", "-c 3", "#{host}",:out=>"/dev/null",err: :out)
          }

          [pid,host.to_s]
          
        end
        
        statuses.each { |p,h|
          
          Process.wait(p)

          printf("%s\n","Host found #{h}") if $?.exitstatus.zero?
          
        }  

        puts "Fine main process #{Process.pid}"      
        
      }
      
      puts "##############################"
      puts "Total Elapsed " + "#{total_elapsed}"
      puts "##############################"
    end
    
  end

end
