module NetscanCalife

  class Utils

    def self.valid_ipnetmask?(netmask)
      /^\d{,3}\.\d{,3}\.\d{,3}\.\d{,3}$/.match(netmask)
    end

    def self.valid_ipnetwork?(network)
      /^\d{,3}\.\d{,3}\.\d{,3}\.\d{,3}$/.match(network)
    end
    
  end

end
