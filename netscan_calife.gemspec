# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'netscan_calife/version'

Gem::Specification.new do |spec|
  spec.name          = "netscan_calife"
  spec.version       = NetscanCalife::VERSION
  spec.authors       = ["calife"]
  spec.email         = ["califerno@gmail.com"]

#  This allow push to rubygems.org
  # if spec.respond_to?(:metadata)
  #   spec.metadata['allowed_push_host'] = "https://rubygems.org/"
  # end

  spec.summary       = %q{Netscan Calife}
  spec.description   = %q{This gem allow to performa a network ping scan}
  spec.homepage      = "https://bitbucket.org/calife/netscan_calife"
  spec.license       = "MIT"

#  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }

#  spec.files = `git ls-files`.split("
#")

  spec.files         = `git ls-files`.split($/)
  
  spec.bindir        = "bin"
  spec.executables   << 'netscan_calife'
  
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.8"
  spec.add_development_dependency "rake", "~> 10.0"

end
