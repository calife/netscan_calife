# NetscanCalife

Netscan application

## Build the gem file

Using Bundler with Rubygem gemspecs
If you're creating a gem from scratch, you can use bundler's built in gem skeleton to create a base gem for you to edit.

% bundle gem netscan_calife

% gem build netscan_calife.gemspec
Successfully built RubyGem
Name: netscan_calife
Version: 0.0.0
File: netscan_calife-0.1.0.gem

% gem install netscan_calife-0.1.0.gem
Successfully installed netscan_calife-0.1.0
1 gem installed

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'netscan_calife'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install netscan_calife

## Usage

./netscan_calife -n 10.10.203.0 -m 255.255.255.0

## Development

push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

1. Fork it ( https://bitbucket.org/calife/netscan_calife )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
